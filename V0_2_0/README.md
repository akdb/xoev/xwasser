# XWasser Artefakte

Hier sind die aktuelle Version der
[XWasser Spezifikation](XWasser-Spezifikation_v0.2.0_2024-05-21.pdf "XWasser
Spezifikation Version 0.2.0") sowie die dazugehörigen XSD-Dateien und
Codelisten zu finden.


## Releases

Version: 0.2.0

Mit dieser Version werden die initialen Fachobjekte und Nachrichten bereitgestellt.
