# Standard XWasser

Der XÖV **Datenaustauschstandard** **`XWasser`** dient dem digitalen Datenaustausch zwischen Betreibern von Wasserversorgungsanlagen (Betreiber), die für regelmäßige Trinkwasseruntersuchungen gemäß den Qualitätsstandards der Trinkwasserverordnung (TrinkwV) verantwortlich sind, und den zuständigen Gesundheitsämtern (GA) sowie weiteren Behörden und diagnostischen Dienstleistern. 

Der Standard deckt somit auch direkte Übermittlungen von Prüfberichten von zugelassenen Untersuchungsstellen (Labor) an die Gesundheitsämter sowie die berichtspflichtige jährliche Übermittlung der Trinkwasserqualitätsdaten durch die Gesundheitsämter an die zuständige obere/oberste Landesbehörde (OLB) ab.

Der Standard **`XWasser`** ist Teil des Projektes [„Bundesweit einheitlicher digitaler Datenaustausch zur Trinkwasserhygiene (SHAPTH)“](https://gesundheitsamt-2025.de/projekte/projektvorstellungen/bundesweit-einheitlicher-digitaler-datenaustausch-zur-trinkwasserhygiene) aller 16 Bundesländer im Rahmen des Paktes für den Öffentlichen Gesundheitsdienst (ÖGD).

Die XML Schema-Definitionen eines XÖV-Standards basieren auf der von dem W3C
empfohlenen XML Schema-Definitionssprache. Alle im Rahmen eines XÖV-Standards
definierten globalen XML-Elemente und benannten XML-Typen müssen sich in einem
Namensraum befinden, der den betroffenen XÖV-Standard eindeutig identifiziert.
Jede XML Schema-Definition eines XÖV-Standards muss versioniert sein.

Das XÖV-Handbuch empfiehlt, den physischen Speicherort einer XML
Schema-Definition in Form einer öffentlichen URL anzugeben. Die hier
(zukünftig) vorliegende versionierte Ablage der **`XWasser`**
Schema-Definitionen dient genau dem Zweck, einen öffentlicher Zugang zu den XML
Schema-Definitionen des XÖV-Standards **`XWasser`**  bereitzustellen und XML
Schema-Validatoren einen direkten Zugriff zu ermöglichen.

## SHAPTH

Das Ziel des Projekts SHAPTH ist die Entwicklung eines gemeinsamen Standards, sowie
einer zentralen Plattform für den Austausch von Daten im Bereich der Trinkwasserhygiene.
Diese Plattform vernetzt alle relevanten Akteure wie Betreibern, Behörden und
zugelassenen Untersuchungsstellen, um den Herausforderungen der Digitalisierung zu
begegnen.

Eine Schnittstellenbeschreibung befindet sich auf der [Projektseite von SHAPTH](https://gitlab.opencode.de/akdb/shapth).

## Releases

Version: 0.8.0


Releases des Standards **`XWasser`** werden/sind im
[XRepository](https://www.xrepository.de/details/urn:xoev-de:lgl:standard:xwasser)
abgelegt. Der Herausgeber ist die [Bayerisches Landesamt für Gesundheit und
Lebensmittelsicherheit](https://www.lgl.bayern.de/) - Gl1 Hygiene (LGL). Die
**`XWasser`** Implementierung erfolgt durch die [AKDB - Anstalt für Kommunale
Datenverarbeitung in Bayern](https://www.akdb.de/).

Die Releases des Standards **`XWasser`** umfassen folgende Bestandteile:

- **UML-Fachmodell (XMI)**:
  Das UML-Modell enthält die Datenstrukturen mit den entsprechenden semantischen Definitionen.

- **XML-Schema-Definitionen (XSD):**
  Die XML-Schemadateien definieren das XML-Format der **`XWasser`** Antragsdaten.

- **Spezifikation (PDF):**
  Die **`XWasser`** Spezifikation beinhaltet unter anderem das
  Datenstrukturmodell, die Beschreibung der Nachrichten, das Informationsmodell
  und die Codelisten von **`XWasser`**.  Die **`XWasser`** Spezifikation stellt
  im Anhang F die Versionshistorie zur Verfügung. Die Unterkapitel von Anhang F
  beinhalten die Release Notes für alles bisherigen **`XWasser`** Versionen.
  Dort werden alle Änderungen zur jeweiligen **`XWasser`** Vorgängerversion
  aufgelistet. 

## Lizenzen

Die XÖV Standards sind, entsprechend dem XÖV Handbuch, offene und
lizenzkostenfreie Standards, die allen Interessierten frei zugänglich zur
Verfügung stehen, somit Open-Source.

## Kontakt

Mailkontakt: xwasser@akdb.de
